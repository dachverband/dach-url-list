import json 
import os

import jsonschema

def validate_groups(group_schema_file="group_schema.json",groupfolder="groups"):
  with open(group_schema_file, "rb") as schema_fp:
    schema_json = json.load(schema_fp)
  for groupfile in os.listdir(groupfolder):
    if not groupfile.endswith(".json"):
      continue
    with open(os.path.join(groupfolder,groupfile),"rb") as group_fp:
      group_json = json.load(group_fp)
    jsonschema.validate(instance=group_json, schema=schema_json)
    
if __name__ == "__main__":
  validate_groups()